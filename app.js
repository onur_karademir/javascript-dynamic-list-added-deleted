// ****** SELECT ITEMS **********
const form = document.querySelector(".main-form");

const deleteAllBtn = document.querySelector(".delete-all-btn");

const itemList = document.querySelector(".item-list");

const submitBtn = document.querySelector(".submit-btn");

const input = document.getElementById("input-id");

const alert = document.querySelector(".my-alert");

// Edit Option //
let editFlag = false;
let editID = "";
let editElement;
// ****** EVENT LISTENERS **********
form.addEventListener("submit", submitHendler);
deleteAllBtn.addEventListener("click", deleteAllItems)
// ****** FUNCTIONS **********
function submitHendler(e) {
  e.preventDefault();
  const value = input.value;
  const id = new Date().getTime().toString();
  if (value !== "" && editFlag === false) {
    const element = document.createElement("article");
    const attr = document.createAttribute("data-id");
    attr.value = id;
    element.setAttributeNode(attr);
    element.classList.add("dynamic-item", "mb-3");
    element.innerHTML = `<p class="title">${value}</p>
    <div class="btn-container">
    <button class="btn btn-success btn-sm edit-btn">Edit</button>
    <button class="btn btn-danger btn-sm delete-btn">Delete</button>
    </div>`;
    itemList.appendChild(element);
    deleteAllBtn.classList.remove("hide-btn");
    displayAlert("Item Added", "success")
    const deleteBtn = document.querySelector(".delete-btn");
    deleteBtn.addEventListener("click", deleteItem)
    const editBtn = document.querySelector(".edit-btn");
    editBtn.addEventListener("click", editItem)
  } else if (value !== "" && editFlag === true) {
    editElement.innerHTML = value;
    displayAlert("Value Changed", "success");
  } else {
  }
}
function displayAlert(text,alertColor) {
  alert.innerText = text;
  alert.classList.add(`my-alert-${alertColor}`);
  setTimeout(function(){
    alert.classList.remove(`my-alert-${alertColor}`)
    alert.innerText = ""
  },1500)
}

function deleteAllItems(e) {
  const elementItem = document.querySelectorAll(".dynamic-item");
  if (elementItem.length > 0) {
    elementItem.forEach(function(item){
        itemList.removeChild(item);
        displayAlert("All Item Deleted", "warning")
    })
  }
  deleteAllBtn.classList.add("hide-btn");
}
function deleteItem(e) {
  const element = e.currentTarget.parentElement.parentElement;
  itemList.removeChild(element);
  displayAlert("item removed", "danger");
}

function editItem(e) {
  const element = e.currentTarget.parentElement.parentElement;
  editElement = e.currentTarget.parentElement.previousElementSibling;
  input.value = editElement.innerHTML;
  editID = element.dataset.id;
  editFlag = true;
  submitBtn.innerText = "Edit"
}

// ****** LOCAL STORAGE **********

// ****** SETUP ITEMS **********
